import { ApiProperty, ApiResponseProperty } from "@nestjs/swagger"
import { TonProofItemReplySuccess } from "@tonconnect/protocol"
import { IsString } from "class-validator"

export class LoginRequestDto_WalletInfo {
    @ApiProperty()
    @IsString()
    address: string

    @ApiProperty()
    @IsString()
    chain: string

    @ApiProperty()
    @IsString()
    walletStateInit: string

    @ApiProperty()
    @IsString()
    publicKey: string
}
export class LoginRequestDto {
    @ApiProperty({ type: LoginRequestDto_WalletInfo })
    walletInfo: LoginRequestDto_WalletInfo

    @ApiProperty()
    tonProof: TonProofItemReplySuccess
}

export class LoginResponseDto {
    @ApiResponseProperty()
    accessToken: string
}
