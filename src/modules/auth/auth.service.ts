import { HttpException, HttpStatus, Inject, Injectable } from "@nestjs/common"
import { ConfigType } from "@nestjs/config"
import { JwtService } from "@nestjs/jwt"
import { TonProofItemReplySuccess } from "@tonconnect/protocol"
import { createHash } from "crypto"
import { WINSTON_MODULE_PROVIDER } from "nest-winston"
import { MESSAGE_CODES } from "src/common/constants"
import { ResponseType } from "src/common/dtos"
import { AppConfig, AuthConfig } from "src/configs"
import { Address } from "ton-core"
import TonWeb from "tonweb"
import nacl from "tweetnacl"
import { Logger } from "winston"
import { LoginResponseDto } from "./dtos"

const tonProofPrefix = "ton-proof-item-v2/"
const tonConnectPrefix = "ton-connect"

interface Domain {
    lengthBytes: number
    value: string
}

interface ParsedMessage {
    workchain: number
    address: Buffer
    timestamp: number
    domain: Domain
    signature: Buffer
    payload: string
    stateInit: string
}

@Injectable()
export class AuthService {
    constructor(
        private jwtService: JwtService,
        @Inject(WINSTON_MODULE_PROVIDER)
        private readonly logger: Logger,
        @Inject(AuthConfig.KEY)
        readonly authConfig: ConfigType<typeof AuthConfig>,
        @Inject(AppConfig.KEY)
        readonly appConfig: ConfigType<typeof AppConfig>
    ) {}

    async login(req: { walletInfo: { address: string; chain: string; walletStateInit: string; publicKey: string }; tonProof: TonProofItemReplySuccess }): Promise<ResponseType<LoginResponseDto>> {
        const walletInfo = req.walletInfo
        const walletAddress = Address.parse(walletInfo.address).toRawString()

        const proof = req.tonProof as TonProofItemReplySuccess
        if (!proof) {
            throw new HttpException("Ton proof is required", HttpStatus.BAD_REQUEST, {
                cause: {
                    code: MESSAGE_CODES.TON_PROOF_REQUIRED
                }
            })
        }

        if (proof.proof.timestamp < (Date.now() - this.authConfig.expiresIn) / 1000) {
            throw new HttpException("Ton proof is expired", HttpStatus.BAD_REQUEST, {
                cause: {
                    code: MESSAGE_CODES.TON_PROOF_EXPIRED
                }
            })
        }

        const parsedMessage = this.convertTonProofMessage(walletInfo, proof)
        const checkMessage = await this.createMessage(parsedMessage)

        const verifyRes = this.signatureVerify(Buffer.from(this.getPublicKey(walletInfo.walletStateInit), "hex"), checkMessage, parsedMessage.signature)
        if (!verifyRes) {
            throw new HttpException("Signature verification failed", HttpStatus.BAD_REQUEST, {
                cause: {
                    code: MESSAGE_CODES.TON_PROOF_INVALID
                }
            })
        }

        const { accessToken } = this.generateToken({ walletAddress })

        return {
            code: MESSAGE_CODES.SUCCESS,
            data: {
                accessToken
            }
        }
    }

    signatureVerify(pubkey: Buffer, message: Buffer, signature: Buffer): boolean {
        return nacl.sign.detached.verify(message, signature, pubkey)
    }

    async createMessage(message: ParsedMessage): Promise<Buffer> {
        const wc = Buffer.alloc(4)
        wc.writeUint32BE(message.workchain)

        const ts = Buffer.alloc(8)
        ts.writeBigUint64LE(BigInt(message.timestamp))

        const dl = Buffer.alloc(4)
        dl.writeUint32LE(message.domain.lengthBytes)

        const m = Buffer.concat([Buffer.from(tonProofPrefix), wc, message.address, dl, Buffer.from(message.domain.value), ts, Buffer.from(message.payload)])

        const messageHash = createHash("sha256").update(m).digest()

        const fullMes = Buffer.concat([Buffer.from([0xff, 0xff]), Buffer.from(tonConnectPrefix), Buffer.from(messageHash)])

        const res = createHash("sha256").update(fullMes).digest()
        return Buffer.from(res)
    }

    convertTonProofMessage(walletInfo: { address: string; chain: string; walletStateInit: string; publicKey: string }, tp: TonProofItemReplySuccess): ParsedMessage {
        const address = Address.parse(walletInfo.address)

        const res: ParsedMessage = {
            workchain: address.workChain,
            address: address.hash,
            domain: {
                lengthBytes: tp.proof.domain.lengthBytes,
                value: tp.proof.domain.value
            },
            signature: Buffer.from(tp.proof.signature, "base64"),
            payload: tp.proof.payload,
            stateInit: walletInfo.walletStateInit,
            timestamp: tp.proof.timestamp
        }
        return res
    }

    generateToken(payload: Record<string, any>): { accessToken: string } {
        const accessToken = this.jwtService.sign(payload, {
            secret: this.authConfig.secret,
            expiresIn: this.authConfig.expiresIn / 1000
        })
        return { accessToken }
    }

    getPublicKey(stateInit: string): string {
        // ref: https://docs.ton.org/develop/dapps/ton-connect/integration
        const state_init = TonWeb.boc.Cell.oneFromBoc(TonWeb.utils.base64ToBytes(stateInit))

        return TonWeb.utils.bytesToHex(state_init.refs[1].bits.array.slice(8, 8 + 32))
    }
}
