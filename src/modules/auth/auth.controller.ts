import { Body, Controller, HttpCode, HttpStatus, Post } from "@nestjs/common"
import { ApiTags } from "@nestjs/swagger"
import { Public } from "src/common"
import { ResponseType } from "src/common/dtos"
import { AuthService } from "./auth.service"
import { LoginRequestDto, LoginResponseDto } from "./dtos"

@Controller("auth")
@ApiTags("Auth")
export class AuthController {
    constructor(private readonly authService: AuthService) {}

    @Public()
    @HttpCode(HttpStatus.OK)
    @Post("login")
    async login(@Body() body: LoginRequestDto): Promise<ResponseType<LoginResponseDto>> {
        return this.authService.login(body)
    }
}
