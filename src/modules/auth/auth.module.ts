import { Module } from "@nestjs/common"
import { ConfigModule, ConfigService } from "@nestjs/config"
import { JwtModule } from "@nestjs/jwt"
import { AuthController } from "./auth.controller"
import { AuthService } from "./auth.service"

@Module({
    imports: [
        JwtModule.registerAsync({
            global: true,
            useFactory: (configService: ConfigService) => {
                return {
                    secret: configService.get<string>("AUTH_SECRET"),
                    signOptions: {
                        expiresIn: configService.get<number>("AUTH_EXPIRES_IN")! / 1000
                    }
                }
            },
            imports: [ConfigModule],
            inject: [ConfigService]
        })
    ],
    controllers: [AuthController],
    exports: [AuthService],
    providers: [AuthService]
})
export class AuthModule {}
