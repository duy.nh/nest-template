import { HttpModule } from "@nestjs/axios"
import { Module } from "@nestjs/common"
import { ConfigModule } from "@nestjs/config"
import { ScheduleModule } from "@nestjs/schedule"
import { RedisModule } from "../redis/redis.module"
import { WorkersService } from "./worker.service"

@Module({
    imports: [ScheduleModule.forRoot(), HttpModule, ConfigModule, RedisModule],
    providers: [WorkersService],
    exports: [WorkersService]
})
export class WorkerModule {}
