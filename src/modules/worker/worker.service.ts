import { HttpService } from "@nestjs/axios"
import { Inject, Injectable, Logger } from "@nestjs/common"
import { ConfigType } from "@nestjs/config"
import { Cron, CronExpression } from "@nestjs/schedule"
import { Address, Cell } from "@ton/core"
import { AxiosError } from "axios"
import { WINSTON_MODULE_PROVIDER } from "nest-winston"
import { catchError, firstValueFrom, timestamp } from "rxjs"
import { AirdropConfig, TonApiConfig } from "src/configs"
import TonWeb from "tonweb"
import { AirdropEntity } from "../airdrop/airdrop.service"
import { RedisService } from "../redis/redis.service"

@Injectable()
export class WorkersService {
    constructor(
        @Inject(WINSTON_MODULE_PROVIDER) private readonly logger: Logger,
        @Inject(AirdropConfig.KEY)
        private readonly airdropConfig: ConfigType<typeof AirdropConfig>,
        private readonly redisService: RedisService,
        @Inject(TonApiConfig.KEY)
        private readonly tonApiConfig: ConfigType<typeof TonApiConfig>,
        private readonly httpService: HttpService
    ) {}

    @Cron(CronExpression.EVERY_5_SECONDS)
    async handleFetchClaimedTransactionCron() {
        console.log("fetch claimed transaction cronjob started")

        for (const merkle of this.airdropConfig.merkleRoots) {
            try {
                const walletAddress = `0:${TonWeb.utils.bytesToHex(Address.parse(merkle.airdropContractAddress).hash)}`
                console.log("airdrop address:", walletAddress)
                const lastFetchLt = await this.redisService.get("worker:last_fetch_lt")
                const { data } = await firstValueFrom(
                    this.httpService
                        .get(`${this.tonApiConfig.apiUrl}/v2/blockchain/accounts/${walletAddress}/transactions`, {
                            headers: {
                                "Content-Type": "application/json",
                                "Authorization": `Bearer ${this.tonApiConfig.apiKey}`
                            },
                            params: {
                                after_lt: lastFetchLt,
                                limit: 100,
                                sort_order: "desc"
                            }
                        })
                        .pipe(
                            catchError((error: AxiosError) => {
                                console.log(error.response?.data)
                                this.logger.error(`fetch ton transaction by address ${walletAddress} error from ton api: `, error)
                                throw "TON Api: An error happened!"
                            })
                        )
                )

                await Promise.all(
                    data.transactions.map(async (transaction: any) => {
                        if (transaction.success && transaction.in_msg.msg_type == "int_msg" && transaction.in_msg.op_code == "0x43c7d5c9") {
                            if (transaction.out_msgs && transaction.out_msgs.length > 0 && transaction.out_msgs[0].msg_type == "int_msg" && transaction.out_msgs[0].op_code == "0x00000001") {
                                const rawBody = transaction.out_msgs[0].raw_body || ""
                                const rawBodyCell = Cell.fromBase64(TonWeb.utils.bytesToBase64(TonWeb.utils.hexToBytes(rawBody)))
                                const rawBodySlice = rawBodyCell.beginParse()
                                const opCode = rawBodySlice.loadUint(32)
                                const queryId = rawBodySlice.loadUint(64)
                                const mintAmount = rawBodySlice.loadUint(64)
                                const mintType = rawBodySlice.loadUint(8)
                                const minter = rawBodySlice.loadAddressAny()

                                console.log("minter friendly:", minter?.toString())

                                if (minter) {
                                    const airdropEntity = await this.redisService.get<AirdropEntity>(`airdrop:${Address.parse(minter.toString()).toRawString().replace("0:", "")}:${Address.parse(walletAddress).toRawString().replace("0:", "")}`)
                                    if (airdropEntity) {
                                        console.log("minter:" ,Address.parse(minter.toString()).toRawString().replace("0:", ""))
                                        console.log("airdrop entity found", airdropEntity)
                                        if (airdropEntity.isClaimed) {
                                            console.log("airdrop entity is already claimed")
                                            return null
                                        } else {
                                            console.log("airdrop entity is not claimed")
                                            await this.redisService.set(`airdrop:${Address.parse(minter.toString()).toRawString().replace("0:", "")}:${Address.parse(walletAddress).toRawString().replace("0:", "")}`, {
                                                amount: airdropEntity.amount,
                                                proof: airdropEntity.proof,
                                                index: airdropEntity.index,
                                                isClaimed: true,
                                                timestamp: airdropEntity.timestamp
                                            })

                                            console.log(await this.redisService.get<AirdropEntity>(`airdrop:${Address.parse(minter.toString()).toRawString().replace("0:", "")}:${Address.parse(walletAddress).toRawString().replace("0:", "")}`))
                                            return transaction
                                        }
                                    }
                                }
                            }
                        }

                        await this.redisService.set("worker:last_fetch_lt", data.transactions[0].lt)
                    })
                )
            } catch (error) {
                console.log(error)
            }
        }
    }
}
