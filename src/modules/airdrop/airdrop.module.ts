import { Module } from "@nestjs/common"
import { ConfigModule } from "@nestjs/config"
import { RedisModule } from "../redis/redis.module"
import { AirdropController } from "./airdrop.controller"
import { AirdropService } from "./airdrop.service"
import { HttpModule } from "@nestjs/axios"
import { CacheModule } from '@nestjs/cache-manager';

@Module({
    imports: [ConfigModule, RedisModule, HttpModule,
        CacheModule.register({
            ttl: 60000, // seconds
            max: 10, // maximum number of items in cache
          })
    ],
    controllers: [AirdropController],
    exports: [AirdropService],
    providers: [AirdropService]
})
export class NftLandModule {}
