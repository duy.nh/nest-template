import { Controller, Get, HttpCode, HttpStatus, Query, UseGuards } from "@nestjs/common"
import { ApiBearerAuth, ApiTags } from "@nestjs/swagger"
import { ApiOkResponseCustom, AuthGuard, CurrentUser, Public } from "src/common"
import { ResponseType } from "src/common/dtos"
import { AirdropService } from "./airdrop.service"
import { AirdropItemResponseDto, GetClaimAirdropParamsRequestDto, GetClaimAirdropParamsResponseDto } from "./dtos"
import { UseInterceptors } from "@nestjs/common";
import { CacheInterceptor } from "@nestjs/cache-manager"

@Controller("airdrop")
@ApiTags("Airdrop")
export class AirdropController {
    constructor(private readonly airdropService: AirdropService) {}

    @UseGuards(AuthGuard)
    @ApiBearerAuth()
    @HttpCode(HttpStatus.OK)
    @Get("claim-airdrop-params")
    @ApiOkResponseCustom(ResponseType, GetClaimAirdropParamsResponseDto)
    async getClaimAirdropParams(@CurrentUser("walletAddress") walletAddress: string, @Query() query: GetClaimAirdropParamsRequestDto): Promise<ResponseType<GetClaimAirdropParamsResponseDto>> {
        console.log(walletAddress)
        return this.airdropService.getClaimAirdropParams({ walletAddress: walletAddress, ...query })
    }

    @UseGuards(AuthGuard)
    @ApiBearerAuth()
    @HttpCode(HttpStatus.OK)
    @Get("airdrop-list")
    @ApiOkResponseCustom(ResponseType, AirdropItemResponseDto, { isArray: true })
    async getAirdropList(@CurrentUser("walletAddress") walletAddress: string): Promise<ResponseType<AirdropItemResponseDto[]>> {
        return this.airdropService.getListAirdrop({ walletAddress })
    }

    
    @Public()
    @UseInterceptors(CacheInterceptor)
    @HttpCode(HttpStatus.OK)
    @Get("nft-info")
    async getNftInfo(): Promise<ResponseType<any>> {
        return this.airdropService.getNftInfo()
    }

    @UseGuards(AuthGuard)
    @ApiBearerAuth()
    @HttpCode(HttpStatus.OK)
    @UseInterceptors(CacheInterceptor)
    @Get("my-nfts")
    async getListNft(@CurrentUser("walletAddress") walletAddress: string): Promise<ResponseType> {
        return this.airdropService.getListNftItem({ walletAddress })
    }

}
