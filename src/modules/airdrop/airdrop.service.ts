import { HttpException, HttpStatus, Inject, Injectable, OnModuleInit } from "@nestjs/common"
import { ConfigType } from "@nestjs/config"
import { Address, Builder, Cell, Dictionary, Slice, beginCell, contractAddress } from "@ton/core"
import { WINSTON_MODULE_PROVIDER } from "nest-winston"
import { MESSAGE_CODES } from "src/common/constants"
import { ResponseType } from "src/common/dtos"
import { AirdropConfig, TonApiConfig } from "src/configs"
import { Logger } from "winston"
import { RedisService } from "../redis/redis.service"
import { AirdropItemResponseDto, GetClaimAirdropParamsResponseDto } from "./dtos"
import { catchError, firstValueFrom } from "rxjs"
import { HttpService } from "@nestjs/axios"
import { AxiosError } from "axios"

export type AirdropEntry = {
    address: Address
    amount: number
}

export interface AirdropEntity {
    amount: string
    airdropContractAddress: string
    index: number
    proof: string
    isClaimed: boolean
    timestamp: number
}

export const airdropEntryValue = {
    serialize: (src: AirdropEntry, buidler: Builder) => {
        buidler.storeAddress(src.address).storeUint(src.amount, 64)
    },
    parse: (src: Slice) => {
        return {
            address: src.loadAddress(),
            amount: src.loadUint(64)
        }
    }
}

@Injectable()
export class AirdropService implements OnModuleInit {
    constructor(
        @Inject(WINSTON_MODULE_PROVIDER)
        private readonly logger: Logger,
        @Inject(AirdropConfig.KEY)
        private readonly airdropConfig: ConfigType<typeof AirdropConfig>,
        private readonly redisService: RedisService,
        @Inject(TonApiConfig.KEY)
        private readonly tonApiConfig: ConfigType<typeof TonApiConfig>,
        private readonly httpService: HttpService
    ) {}

    async onModuleInit() {
        // const keys = await this.redisService.keys(`airdrop:*`)
        // await Promise.all(keys.map(async key => {
        //     return await this.redisService.del(key)
        // }))

        await this.uploadAirdropDataOnRedis()
    }

    async uploadAirdropDataOnRedis() {
        for (const merkle of this.airdropConfig.merkleRoots) {
            const keys = await this.redisService.keys(`airdrop:*:${Address.parse(merkle.airdropContractAddress).toRawString().replace("0:", "")}`)
            if (keys.length == 0) {
                const dictCell = Cell.fromBase64(merkle.base64)
                const dict = dictCell.beginParse().loadDictDirect(Dictionary.Keys.BigUint(256), airdropEntryValue)

                for (let i = 0; i < merkle.length; i++) {
                    const element = dict.get(BigInt(i))

                    if (element) {
                        // airdrop:user_address:airdrop_address {amount, proof, index}
                        await this.redisService.set(`airdrop:${element.address.toRawString().replace("0:", "")}:${Address.parse(merkle.airdropContractAddress).toRawString().replace("0:", "")}`, {
                            amount: element.amount.toString(),
                            proof: dict.generateMerkleProof(BigInt(i)).toBoc().toString("base64"),
                            index: i,
                            isClaimed: false,
                            timestamp: Date.now()
                        })
                    }
                }
            }
        }
    }

    async getClaimAirdropParams(request: { walletAddress: string; airdropAddress: string }): Promise<ResponseType<GetClaimAirdropParamsResponseDto>> {
        const airdropEntity = await this.redisService.get<AirdropEntity>(`airdrop:${Address.parse(request.walletAddress).toRawString().replace("0:", "")}:${Address.parse(request.airdropAddress).toRawString().replace("0:", "")}`)
        console.log(airdropEntity)
        if (!airdropEntity) {
            throw new HttpException("getClaimAirdropParams: airdrop entity not found", HttpStatus.BAD_REQUEST, {
                cause: {
                    code: MESSAGE_CODES.YOU_ARE_NOT_ELIGIBLE
                }
            })
        }

        try {
            const airdropHelperCodeCell = Cell.fromBase64(this.airdropConfig.airdropHelperCode)
            const airdropHelperDataCell = beginCell()
                .storeBit(false)
                .storeAddress(Address.parse(request.airdropAddress))
                .storeBuffer(Buffer.from(Cell.fromBase64(airdropEntity.proof).hash()), 32)
                .storeUint(airdropEntity.index, 256)
                .endCell()

            const airdropStateInitCell = beginCell().storeUint(6, 5).storeRef(airdropHelperCodeCell).storeRef(airdropHelperDataCell).endCell()

            return {
                code: MESSAGE_CODES.SUCCESS,
                data: {
                    helperAddress: contractAddress(0, {
                        code: airdropHelperCodeCell,
                        data: airdropHelperDataCell
                    }).toString(),
                    stateInit: airdropStateInitCell.toBoc().toString("base64"),
                    proof: airdropEntity.proof,
                    amount: airdropEntity.amount
                }
            }
        } catch (error) {
            this.logger.error(error)
            throw new HttpException("getClaimAirdropParams: An error happened!", HttpStatus.INTERNAL_SERVER_ERROR, {
                cause: {
                    code: MESSAGE_CODES.INTERNAL_SERVER_ERROR
                }
            })
        }
    }

    async getListAirdrop(request: { walletAddress: string }): Promise<ResponseType<AirdropItemResponseDto[]>> {
        try {
            const keys = await this.redisService.keys(`airdrop:${Address.parse(request.walletAddress).toRawString().replace("0:", "")}:*`)
            const airdrops = await Promise.all(
                keys.map(async (key) => {
                    const airdropEntity = await this.redisService.get<AirdropEntity>(key)
                    if (!airdropEntity) {
                        return null
                    }

                    return {
                        airdropAddress: `0:${key.split(":")[2]}`,
                        amount: airdropEntity.amount,
                        isClaimed: airdropEntity.isClaimed,
                        timestamp: airdropEntity.timestamp
                    }
                })
            )

            return {
                code: MESSAGE_CODES.SUCCESS,
                data: airdrops.filter((airdrop) => airdrop != null).sort((a:any, b:any) => b.timestamp - a.timestamp) as any
            }
        } catch (error) {
            this.logger.error(error)
            throw new HttpException("getListAirdrop: An error happened!", HttpStatus.INTERNAL_SERVER_ERROR, {
                cause: {
                    code: MESSAGE_CODES.INTERNAL_SERVER_ERROR
                }
            })
        }
    }

    async getNftInfo(){
        return {
            code: MESSAGE_CODES.SUCCESS,
            data: {
                address: this.airdropConfig.nftCollectionAddress,
                name:"$CATGM Pre-Market",
                description:"$CATGM Pre-Market Vouchers, which correspond to in-game $wCATGM currency, can be exchanged for $CATGM after the token generation event.",
                image:"https://nft.catgoldminer.ai/catgm-voucher-logo.png",
                cover_image: `https://nft.catgoldminer.ai/catgm-voucher-cover.png`,
                social_links:[
                    "https://www.catgoldminer.ai",
                    "https://twitter.com/catgoldminer",
                    "https://discord.gg/RdfSasXVJN",
                    "https://t.me/catgoldminerbot",
                    "https://t.me/catgoldminerann",
                    "https://t.me/catgoldminerchat",
                    "https://vk.com/catgoldminer"           
                ] ,
                nft_item_metadata: {
                    name:`Voucher 10000 CATGM`,
                    description:"After the token generation event, you can exchange this NFT Voucher for $CATGM tokens.",
                    image:`https://nft.catgoldminer.ai/catgm-voucher-item.png`,
                    attributes:[{value:"10000"}]
                }
            }
        }
    }

    async getListNftItem(request: {walletAddress: string}){
        try {
            const {data} = await firstValueFrom(
                this.httpService
                    .get(`${this.tonApiConfig.apiUrl}/v2/accounts/${request.walletAddress}/nfts`, {
                        headers: {
                            "Content-Type": "application/json",
                            "Authorization": `Bearer ${this.tonApiConfig.apiKey}`
                        },
                        params: {
                            collection: this.airdropConfig.nftCollectionAddress,
                            limit: 100
                        }
                    })
                    .pipe(
                        catchError((error: AxiosError) => {
                            console.log(error.response?.data)
                            this.logger.error(`fetch ton transaction by address ${request.walletAddress} error from ton api: `, error)
                            throw "TON Api: An error happened!"
                        })
                    )
            
            )

            return {
                code: MESSAGE_CODES.SUCCESS,
                data: data.nft_items
            }
        } catch (error) {
            this.logger.error(error)
            throw new HttpException("getListAirdrop: An error happened!", HttpStatus.INTERNAL_SERVER_ERROR, {
                cause: {
                    code: MESSAGE_CODES.INTERNAL_SERVER_ERROR
                }
            })
        }
    }
}
