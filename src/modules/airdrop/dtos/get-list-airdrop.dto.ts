import { ApiResponseProperty } from "@nestjs/swagger"

export class AirdropItemResponseDto {
    @ApiResponseProperty()
    airdropAddress: string

    @ApiResponseProperty()
    amount: string

    @ApiResponseProperty()
    isClaimed: boolean
}
