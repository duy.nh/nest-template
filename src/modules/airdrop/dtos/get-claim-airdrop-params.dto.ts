import { ApiProperty, ApiResponseProperty } from "@nestjs/swagger"
import { IsString } from "class-validator"

export class GetClaimAirdropParamsResponseDto {
    @ApiResponseProperty()
    helperAddress: string

    @ApiResponseProperty()
    stateInit: string

    @ApiResponseProperty()
    proof: string

    @ApiResponseProperty()
    amount: string
}

export class GetClaimAirdropParamsRequestDto {
    @ApiProperty()
    @IsString()
    airdropAddress: string
}
