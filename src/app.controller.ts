import { Controller, Get, Param } from "@nestjs/common"
import { ConfigService } from "@nestjs/config"

@Controller("")
export class AppController {
    constructor(private readonly configService: ConfigService) {}

    @Get()
    start() {
        return {
            statusCode: 200,
            data: new Date().toISOString() + " - VERSION: " + this.configService.get<string>("VERSION")
        }
    }

    @Get("metadata/catgm-voucher/:tokenId.:ext")
    getNftItemMetadata(@Param("tokenId") tokenId: string) {
        return {
            name:`Voucher 10000 CATGM #${tokenId}`,
            description:"After the token generation event, you can exchange this NFT Voucher for $CATGM tokens.",
            image:`https://nft.catgoldminer.ai/catgm-voucher-item.png`,
            attributes:[{value:"10000"}]
        }
    }

    @Get("metadata/collection/catgm-voucher:ext")
    getNftCollectionMetadata() {
        return {
            name:"$CATGM Pre-Market",
            description:"$CATGM Pre-Market Vouchers, which correspond to in-game $wCATGM currency, can be exchanged for $CATGM after the token generation event.",
            image:"https://nft.catgoldminer.ai/catgm-voucher-logo.png",
            cover_image: `https://nft.catgoldminer.ai/catgm-voucher-cover.png`,
            social_links:[
                "https://www.catgoldminer.ai",
                "https://twitter.com/catgoldminer",
                "https://discord.gg/RdfSasXVJN",
                "https://t.me/catgoldminerbot",
                "https://t.me/catgoldminerann",
                "https://t.me/catgoldminerchat",
                "https://vk.com/catgoldminer"           
            ]   
        }
    }
}
